# Notes

- https://gitlab.com/joaommpalmeiro/template-react-vike-static

## Commands

```bash
npm install \
@vitejs/plugin-react \
react \
react-dom \
vike \
vike-react \
&& npm install -D \
@biomejs/biome \
@joaopalmeiro/biome-react-config \
@types/react \
@types/react-dom \
npm-run-all2 \
sort-package-json \
typescript \
vike-tsconfigs \
vite
```

```bash
rm -rf node_modules/ && npm install
```
