# Template Notes

- https://github.com/joaopalmeiro/template-qwik-city-static
- https://github.com/joaopalmeiro/template-astro
- https://github.com/joaopalmeiro/template-ts-package
- https://codeberg.org/joaopalmeiro/biome-config
- https://codeberg.org/joaopalmeiro/biome-solid-config
- https://vike.dev/prerender:
  - https://vike.dev/github-pages
  - https://vike.dev/static-hosts
- https://batijs.dev/
- https://docs.npmjs.com/cli/v10/commands/npm-init:
  - https://docs.npmjs.com/cli/v10/commands/npm-init#forwarding-additional-options
- https://vike.dev/data-fetching
- https://vike.dev/useData
- https://vike.dev/data
- https://github.com/batijs/bati/tree/v0.0.178/boilerplates/react:
  - https://github.com/batijs/bati/blob/v0.0.178/boilerplates/react/tsconfig.json
  - https://github.com/batijs/bati/blob/v0.0.178/boilerplates/tsconfig.base.json
  - https://github.com/batijs/bati/blob/v0.0.178/tsconfig.json
  - https://github.com/batijs/bati/blob/v0.0.178/boilerplates/react/files/%24tsconfig.json.ts
- https://github.com/vikejs/vike/tree/v0.4.171/examples/react-minimal
- https://github.com/vikejs/vike/tree/v0.4.171/examples/vike-react-simple
- https://github.com/vikejs/vike/tree/v0.4.171/examples/vike-react
- https://vike.dev/react
- https://github.com/vikejs/vike/tree/v0.4.171/boilerplates/boilerplate-react-ts
- https://vitejs.dev/config/#config-intellisense
- https://github.com/batijs/bati/tree/v0.0.178/boilerplates/tailwindcss
- https://vike.dev/cloudflare-workers#universal-fetch
- https://github.com/vikejs/vike-react:
  - https://github.com/vikejs/vike-react/tree/main/packages/vike-react
  - https://github.com/vikejs/vike-react/tree/vike-react%400.4.8/examples/minimal
- https://vike.dev/onPageTransitionStart
- https://biomejs.dev/linter/rules/use-filenaming-convention/:
  - "The filename can start with a dot or a plus sign, be prefixed and suffixed by underscores `_`."
  - "The convention of prefixing a filename with a plus sign is used by Sveltekit and Vike."
  - https://biomejs.dev/internals/changelog/#170-2024-04-15
- https://github.com/biomejs/biome/releases/tag/cli%2Fv1.6.4
- https://github.com/biomejs/biome/releases/tag/cli%2Fv1.7.0
- https://biomejs.dev/reference/configuration/#javascriptjsxruntime
- https://github.com/biomejs/biome/blob/cli/v1.7.0/packages/%40biomejs/biome/configuration_schema.json
- https://github.com/vitejs/vite-plugin-react/blob/v4.3.0/packages/plugin-react/package.json
- https://semver.npmjs.com/
- Changelog files:
  - https://github.com/vikejs/vike/blob/main/CHANGELOG.md
  - https://github.com/vikejs/vike-react/blob/main/packages/vike-react/CHANGELOG.md
  - https://github.com/vitejs/vite-plugin-react/releases
  - https://github.com/vitejs/vite/blob/main/packages/vite/CHANGELOG.md
  - https://github.com/bcomnes/npm-run-all2/blob/master/CHANGELOG.md
- https://github.com/Rich-Harris/degit/issues/279
- https://github.com/unjs/giget
- https://github.com/vikejs/vike-react/tree/vike-react%400.4.8/packages/vike-react
- https://github.com/batijs/bati/blob/v0.0.197/boilerplates/biome/files/biome.json

## Commands

```bash
npm init @batijs/app bati-demo -- --react
```

```bash
rm -rf bati-demo && npm init @batijs/app bati-demo -- --react
```

```bash
cd bati-demo/ && npm install && cd ..
```

```bash
npm init @batijs/app bati-tailwind-demo -- --react --tailwindcss
```
