import React from "react";

export default function HeadDefault() {
  return (
    <>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="author" content="João Palmeiro" />
      <meta
        name="description"
        content="Opinionated React + Vike + SSG template for new projects."
      />
    </>
  );
}
