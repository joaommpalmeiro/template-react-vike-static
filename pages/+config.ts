import vikeReact from "vike-react/config";
import type { Config } from "vike/types";

import Head from "../layouts/HeadDefault";

// Documentation:
// - https://vike.dev/vike-react
// - https://github.com/vikejs/vike-react/blob/vike-react%400.4.11/examples/full/pages/%2Bconfig.ts
export default {
  Head,
  title: "template-react-vike-static",
  extends: vikeReact,
} satisfies Config;
